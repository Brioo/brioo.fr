<?php

namespace App\Controller;

use App\Entity\Contact;
use App\Form\ContactFormType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class HomeController extends AbstractController
{
    #[Route('/home', name: 'app_home')]
    public function index(): Response
    {
        $contact = new Contact();

        $contactForm = $this->createForm(ContactFormType::class, $contact);

        return $this->render('home/index.html.twig', [
            'controller_name' => 'HomeController',
            'contactForm'     => $contactForm->createView(),
        ]);
    }
}
