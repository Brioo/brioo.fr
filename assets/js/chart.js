import inViewport from 'in-viewport';

(function($) {

    inViewport(document.getElementById('skills-carousel'), { offset: 0 }, skillsPictureCarousel);

    const x = window.matchMedia("(max-width: 769px)");
    triggerIfMatch(x); // Call listener function at run time
    x.addListener(triggerIfMatch); // Attach listener function on state changes

    /**
     * With the offset the viewport mobile was not triggering annimation
     * Now it is using the screen width check
     *
     * @param x
     */
    function triggerIfMatch(x) {
        if (x.matches) { // If media query matches
            inViewport(document.getElementById('skills-chart-back'), { offset: -100 }, skillsChartAnnimattion);
            inViewport(document.getElementById('skills-chart-front'), { offset: -100 }, skillsChartAnnimattion);
        } else {
            inViewport(document.getElementById('skills-chart-back'), { offset: -450 }, skillsChartAnnimattion);
            inViewport(document.getElementById('skills-chart-front'), { offset: -450 }, skillsChartAnnimattion);
        }
    }

    /**
     * Trigger annimation on skills chart
     */
    function skillsChartAnnimattion() {
        $('.skillbar').each(function(){
            $(this).find('.skillbar-bar').animate({
                width: $(this).attr('data-percent'),
            }, 5000);
        });
        $('.skillbar-percent').css('visibility', 'visible');
    }

    /**
     * Trigger carousel skills picture
     */
    function skillsPictureCarousel() {
        $('.owl-carousel').owlCarousel({
            autoplay: true,
            autoplayTimeout: 2000,
            dots: false,
            items: 5,
            loop: true,
            margin: 10,
            nav: false,
            responsiveClass:true,
            responsive:{
                0:{
                    items:1,
                },
                600:{
                    items:3,
                },
                1000:{
                    items:5,
                }
            }
        });
    }

})(jQuery);
