/*
 * Welcome to your app's main JavaScript file!
 *
 * This file will be included onto the page via the importmap() Twig function,
 * which should already be in your base.html.twig.
 */
// import '../assets/styles/app.scss';
import './vendor/@fortawesome/fontawesome-free/css/fontawesome.min.css'
import "./vendor/wowjs/css/libs/animate.css";
import "./vendor/tooltipster/dist/css/tooltipster.bundle.min.css";
import "./vendor/tooltipster/dist/css/plugins/tooltipster/sideTip/themes/tooltipster-sideTip-punk.min.css";
import "./vendor/owl.carousel/dist/assets/owl.carousel.css";

import './autoprovide.js';
import './translator.js';
import 'bootstrap';
import 'owl.carousel';
import 'tooltipster';
import 'jquery.easing';

// window.inViewport = require('in-viewport');
// window.Typed =

// Fos Js routing solutions
// const routes = require('./static/fos_js_routes.json');
// const Routing = require('../../vendor/friendsofsymfony/jsrouting-bundle/Resources/public/js/router.min.js');




// global.Routing = Routing;
// global.Translator = Translator;
//
// Routing.setRoutingData(routes);
import './js/main.js';
import './js/chart.js';
import './js/formValidation.js';
import './js/notify.js';
import './js/ajax.js';


import {trans, WELCOME_TIPS1_TITLE, WELCOME_TIPS1_MESSAGE} from './translator.js';



// Popup notify home screen
$(document).ready(function() {
    new WOW().init();

    console.log(trans(WELCOME_TIPS1_TITLE));
    // welcomeNotify(trans(WELCOME_TIPS1_TITLE), trans(WELCOME_TIPS1_MESSAGE))
});

function welcomeNotify(title, message) {


    // console.log(title);
    // $.notify({
    //     animation: 'fade',
    //     theme: 'tooltipster-punk',
    //     title: trans(title),
    //     message: trans(message),
    // }, {
    //     delay: 5000,
    // });
}