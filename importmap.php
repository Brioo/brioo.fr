<?php

/**
 * Returns the importmap for this application.
 *
 * - "path" is a path inside the asset mapper system. Use the
 *     "debug:asset-map" command to see the full list of paths.
 *
 * - "entrypoint" (JavaScript only) set to true for any module that will
 *     be used as an "entrypoint" (and passed to the importmap() Twig function).
 *
 * The "importmap:require" command can be used to add new entries to this file.
 */
return [
    'app' => [
        'path' => './assets/app.js',
        'entrypoint' => true,
    ],
    '@symfony/ux-translator' => [
        'path' => './vendor/symfony/ux-translator/assets/dist/translator_controller.js',
    ],
    '@app/translations' => [
        'path' => './var/translations/index.js',
    ],
    '@app/translations/configuration' => [
        'path' => './var/translations/configuration.js',
    ],
    '@popperjs/core' => [
        'version' => '2.11.8',
    ],
    'jquery.easing' => [
        'version' => '1.4.1',
    ],
    'jquery' => [
        'version' => '3.7.1',
    ],
    'tooltipster' => [
        'version' => '4.2.8',
    ],
    'typed.js' => [
        'version' => '2.1.0',
    ],
    'wowjs' => [
        'version' => '1.1.3',
    ],
    'in-viewport' => [
        'version' => '3.6.0',
    ],
    'owl.carousel' => [
        'version' => '2.3.4',
    ],
    'owl.carousel/dist/assets/owl.carousel.min.css' => [
        'version' => '2.3.4',
        'type' => 'css',
    ],
    'bootstrap' => [
        'version' => '5.3.3',
    ],
    'bootstrap/dist/css/bootstrap.min.css' => [
        'version' => '5.3.3',
        'type' => 'css',
    ],
    'intl-messageformat' => [
        'version' => '10.5.11',
    ],
    'tslib' => [
        'version' => '2.6.2',
    ],
    '@formatjs/icu-messageformat-parser' => [
        'version' => '2.7.6',
    ],
    '@formatjs/fast-memoize' => [
        'version' => '2.2.0',
    ],
    '@formatjs/icu-skeleton-parser' => [
        'version' => '1.8.0',
    ],
    'wowjs/css/libs/animate.css' => [
        'version' => '1.1.3',
        'type' => 'css',
    ],
    'tooltipster/dist/css/tooltipster.bundle.min.css' => [
        'version' => '4.2.8',
        'type' => 'css',
    ],
    'tooltipster/dist/css/plugins/tooltipster/sideTip/themes/tooltipster-sideTip-punk.min.css' => [
        'version' => '4.2.8',
        'type' => 'css',
    ],
    'owl.carousel/dist/assets/owl.carousel.css' => [
        'version' => '2.3.4',
        'type' => 'css',
    ],
];
