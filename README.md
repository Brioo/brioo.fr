



## Deploying
When you deploy, run sass:build command before the asset-map:compile command so the built file is available:
```bash
php bin/console sass:build
php bin/console asset-map:compile
```